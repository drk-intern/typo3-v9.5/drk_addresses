<?php
defined('TYPO3') or die('Access denied.');

# OrganisationList Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_addresses',
    'OrganisationList',
    [
        \DrkService\DrkAddresses\Controller\OrganisationListController::class => 'list, detail',
    ],
    // non-cacheable actions
    [
        \DrkService\DrkAddresses\Controller\OrganisationListController::class => 'detail',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

# OrganisationOverview Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_addresses',
    'OrganisationOverview',
    [
        \DrkService\DrkAddresses\Controller\OrganisationOverviewController::class => 'overview, detail',
    ],
    // non-cacheable actions
    [
        \DrkService\DrkAddresses\Controller\OrganisationOverviewController::class => 'detail',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

# OrganisationSearch Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_addresses',
    'OrganisationSearch',
    [
        \DrkService\DrkAddresses\Controller\OrganisationSearchController::class => 'search, detail',
    ],
    // non-cacheable actions
    [
        \DrkService\DrkAddresses\Controller\OrganisationSearchController::class => 'search, detail',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

# OrganisationSister Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_addresses',
    'OrganisationSister',
    [
        \DrkService\DrkAddresses\Controller\OrganisationSisterController::class => 'sister, detail',
    ],
    // non-cacheable actions
    [
        \DrkService\DrkAddresses\Controller\OrganisationSisterController::class => 'detail',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

# OrganisationCompany Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_addresses',
    'OrganisationCompany',
    [
        \DrkService\DrkAddresses\Controller\OrganisationCompanyController::class => 'company, detail',
    ],
    // non-cacheable actions
    [
        \DrkService\DrkAddresses\Controller\OrganisationCompanyController::class => 'detail',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc']['drkaddresses'] = 'DrkService\DrkAddresses\Hook\T3libTcemainHook->clearCachePostProc';

#$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_drkaddresses_organisationplugin[organisation]';
