<?php
defined('TYPO3') or die('Access denied.');

# OrganisationList Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_addresses',
    'OrganisationList',
    'LLL:EXT:drk_addresses/Resources/Private/Language/locallang_be.xlf:tt_content.organisation_list_plugin.title',
    'EXT:drk_addresses/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Adressen'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

# OrganisationOverview Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_addresses',
    'OrganisationOverview',
    'LLL:EXT:drk_addresses/Resources/Private/Language/locallang_be.xlf:tt_content.organisation_overview_plugin.title',
    'EXT:drk_addresses/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Adressen'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

# OrganisationSearch Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_addresses',
    'OrganisationSearch',
    'LLL:EXT:drk_addresses/Resources/Private/Language/locallang_be.xlf:tt_content.organisation_search_plugin.title',
    'EXT:drk_addresses/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Adressen'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

# OrganisationSister Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_addresses',
    'OrganisationSister',
    'LLL:EXT:drk_addresses/Resources/Private/Language/locallang_be.xlf:tt_content.organisation_sister_plugin.title',
    'EXT:drk_addresses/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Adressen'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

# OrganisationCompany Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_addresses',
    'OrganisationCompany',
    'LLL:EXT:drk_addresses/Resources/Private/Language/locallang_be.xlf:tt_content.organisation_company_plugin.title',
    'EXT:drk_addresses/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Adressen'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);
