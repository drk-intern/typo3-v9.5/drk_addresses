<?php

namespace DrkService\DrkAddresses\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use DRK\DrkGeneral\Utilities\Utility;
use Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class OrganisationRepository extends AbstractDrkRepository
{

    /**
     * @var array
     */
    protected $settings;

    /**
     * @param array $settings
     */
    public function __construct($settings = null)
    {
        if ($settings !== null) {
            $this->settings = $settings;
        } else {
            /** @var ConfigurationManagerInterface $configurationManager */
            $configurationManager = GeneralUtility::makeInstance(ConfigurationManagerInterface::class);
            $this->settings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
        }
    }

    /**
     * @param string $type
     * @param array $excludeIds
     * @return array
     * @throws Exception
     */
    public function findAll(string $type = 'K', array $excludeIds = []): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_addresses|findAll', $type, $excludeIds]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $client = $this->getDldbClient();
        $organisations = $offerLinks = Utility::convertObjectToArray(
            $client->getOrganisationList($type)
        );


        if (empty($organisations)) {
            return [];
        }

        if (!empty($excludeIds)) {
            $organisations = array_filter($organisations, function ($item) use ($excludeIds) {
                return !in_array($item['orgID'], $excludeIds, true);
            });
        }

        $this->getCache()->set($cacheIdentifier, $organisations, [], $this->heavy_cache);

        return $organisations;
    }

    /**
     * @param string $searchString
     * @param string $type
     * @return array
     * @throws Exception
     */
    public function findByName(string $searchString, string $type = 'K'): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_addresses|findByName', $searchString, $type]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $client = $this->getDldbClient();
        $organisations = $offerLinks = Utility::convertObjectToArray(
            $client->getOrganisationListbyName($searchString, $type)
        );

        if (empty($organisations)) {
            return [];
        }

        $this->getCache()->set($cacheIdentifier, $organisations, [], $this->long_cache);

        return $organisations;
    }

    /**
     * @param string $organisationId
     * @return array
     */
    public function findById($organisationId): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_addresses|findById', $organisationId]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $client = $this->getDldbClient();

        // check of valid $organisationId
        if (!is_numeric($organisationId) && strlen($organisationId) < 10) {
            return [];
        }

        // fix lost leading zero for lv
        if (strlen($organisationId) == 3) {
            $organisationId = '0' . $organisationId;
        }

        $organisationData = $client->getOrganisationbyID($organisationId);

        if (empty($organisationData) && !is_object($organisationData)) {
            return [];
        } else {
            $organisation = $offerLinks = Utility::convertObjectToArray($organisationData);
        }

        //init
        $organisation['services'] = [];
        $organisation['courses'] = [];
        $organisation['institutions'] = [];
        $organisation['objects'] = [];
        $organisation['chapters'] = [];
        $organisation['sisters'] = [];
        $organisation['companies'] = [];

        $offerData = $client->getOrganisationOfferLinks($organisationId);

        if (empty($offerData) && !is_object($offerData)) {
            return [];
        } else {
            $offers = $offerLinks = Utility::convertObjectToArray($offerData);
        }

        if (!is_array($offers) || empty($offers)) {
            $organisation['services'] = [];
            $organisation['courses'] = [];
            $organisation['institutions'] = [];
            $organisation['objects'] = [];
        } else {
            usort($offers, function ($a, $b) {
                return strcmp($a['orgOfferDescription'], $b['orgOfferDescription']);
            });
            $organisation['services'] = array_filter($offers, function ($offer) {
                return $offer['orgOfferClass'] === 'D';
            });
            $organisation['courses'] = array_filter($offers, function ($offer) {
                return $offer['orgOfferClass'] === 'K';
            });
            $organisation['institutions'] = array_filter($offers, function ($offer) {
                return $offer['orgOfferClass'] === 'E';
            });
            $organisation['objects'] = array_filter($offers, function ($offer) {
                return $offer['orgOfferClass'] === 'O';
            });
        }

        if ($organisation['orgType'] !== 'L') {
            $organisation['chapters'] = [];
            $organisation['sisters'] = [];
            $organisation['companies'] = [];
        } else {
            $subOrganisations = $offerLinks = Utility::convertObjectToArray(
                $client->getOrganisationbyParentID($organisationId)
            );

            if (empty($subOrganisations)) {
                $organisation['chapters'] = [];
                $organisation['sisters'] = [];
                $organisation['companies'] = [];
            } else {
                usort($subOrganisations, function ($a, $b) {
                    return strcmp($a['orgName'], $b['orgName']);
                });
                $organisation['chapters'] = array_filter($subOrganisations, function ($subOrganisation) {
                    return $subOrganisation['orgType'] === 'K';
                });
                $organisation['sisters'] = array_filter($subOrganisations, function ($subOrganisation) {
                    return $subOrganisation['orgType'] === 'S';
                });
                $organisation['companies'] = array_filter($subOrganisations, function ($subOrganisation) {
                    return $subOrganisation['orgType'] === 'G';
                });
            }
        }

        $this->getCache()->set($cacheIdentifier, $organisation, [], $this->heavy_cache);

        return $organisation;
    }

    /**
     * @param string $sword
     * @return array
     * @throws Exception
     */
    public function findByZipOrCity(string $sword): array
    {

        $cacheIdentifier = sha1(json_encode(['drk_addresses|findByZipOrCity', $sword]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $client = $this->getDldbClient();
        $organisations = $offerLinks = Utility::convertObjectToArray(
            $client->getOrganisationbyZiporCity(trim($sword))
        );

        if (empty($organisations)) {
            return [];
        } else {
            $latLngArray = [];
            $latLngCounter = 0;

            foreach ($organisations as &$organisation) {
                if (empty($organisation['orgLatitude']) || empty($organisation['orgLongitude']))
                {
                    continue;
                }

                $orgGeoHash = $organisation['orgLatitude'] . ':' . $organisation['orgLongitude'];
                if (($latLngIndex = array_search($orgGeoHash, $latLngArray)) !== false) {
                    $currentAsciiChar = $latLngIndex;
                } else {
                    $latLngArray[] = $orgGeoHash;
                    $currentAsciiChar = $latLngCounter++;
                }

                $organisation['Latitude'] = $organisation['orgLatitude'];
                $organisation['Longitude'] = $organisation['orgLongitude'];
                $organisation['markerLabel'] = chr($currentAsciiChar + 65);
            }
        }

        $this->getCache()->set($cacheIdentifier, $organisations, [], $this->long_cache);

        return $organisations;
    }
}
