<?php

namespace DrkService\DrkAddresses\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


use TYPO3\CMS\Extbase\Http\ForwardResponse;

class OrganisationCompanyController extends OrganisationListController
{

    /**
     * Company view
     */
    public function companyAction(): ForwardResponse
    {
        $arguments = [
            'type' => 'G',
        ];
        if ($this->request->hasArgument('section')) {
            $arguments['section'] = $this->request->getArgument('section');
        }

        $e =  new ForwardResponse('list');
        return $e->withArguments($arguments);
    }
}
