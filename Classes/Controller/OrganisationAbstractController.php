<?php

namespace DrkService\DrkAddresses\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DrkService\DrkAddresses\Domain\Repository\OrganisationRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use UnexpectedValueException;
use Psr\Http\Message\ResponseInterface;

class OrganisationAbstractController extends ActionController
{
    /**
     * @var OrganisationRepository
     */
    protected OrganisationRepository $organisationRepository;

    /**
     * @var string
     */
    protected string $defaultSection = 'A';

    /**
     * @var array
     */
    protected array $orgPidKeys = ['K' => 'pIdKv', 'L' => 'pIdLv', 'S' => 'pIdSister', 'G' => 'pIdCompany'];

    /**
     * @param OrganisationRepository $organisationRepository
     */
    public function injectOrganisationRepository(OrganisationRepository $organisationRepository): void
    {
        $this->organisationRepository = $organisationRepository;
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        // check PageID, we need one für detail action
        // we take the pIdKv as default, so this must not be empty!
        if (empty($this->settings['pIdKv']) or intval($this->settings['pIdKv']) == 0) {
            throw new UnexpectedValueException('Constant pIdKv must not be empty');
        } else {
            $this->settings['pIdLv'] = !empty($this->settings['pIdLv']) ? $this->settings['pIdLv'] : $this->settings['pIdKv'];
            $this->settings['pIdSister'] = !empty($this->settings['pIdSister']) ? $this->settings['pIdSister'] : $this->settings['pIdKv'];
            $this->settings['pIdCompany'] = !empty($this->settings['pIdCompany']) ? $this->settings['pIdCompany'] : $this->settings['pIdKv'];
        }
    }

    /**
     * List view
     *
     * @param string $type
     * @return ResponseInterface
     * @throws Exception
     */
    public function listAction(string $type = 'K'): ResponseInterface
    {
        $sections = range('A', 'Z');
        $currentSection = $this->defaultSection;
        if ($this->request->hasArgument('section')) {
            $currentSection = strtoupper($this->request->getArgument('section'));
            if (!in_array($currentSection, $sections, true)) {
                $currentSection = $this->defaultSection;
            }
        }

        $this->view->assignMultiple([
            'type' => $type,
            'sections' => $sections,
            'currentSection' => $currentSection,
            'organisations' => $this->organisationRepository->findByName($currentSection, $type),
        ]);

        $this->view->setTemplatePathAndFilename('EXT:drk_addresses/Resources/Private/Templates/List.html');

        return $this->htmlResponse();
    }

    /**
     * Detail view
     *
     * @param string $organisation
     * @return ResponseInterface
     */
    public function detailAction(string $organisation = ''): ResponseInterface
    {
        if (empty($organisation or intval($organisation) == 0)) {
            $this->view->assign('organisation', null);
        } else {
            $this->view->assign('organisation', $this->organisationRepository->findById($organisation));
        }

        $this->view->setTemplatePathAndFilename('EXT:drk_addresses/Resources/Private/Templates/Detail.html');

        return $this->htmlResponse();
    }

}
