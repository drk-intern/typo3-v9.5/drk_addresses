<?php

namespace DrkService\DrkAddresses\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Exception;
use GeorgRinger\NumberedPagination\NumberedPagination;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Pagination\ArrayPaginator;
use TYPO3\CMS\Extbase\Service\ExtensionService;


class OrganisationSearchController extends OrganisationAbstractController
{
    private $objectManager;

    /**
     * Search view
     * @throws Exception
     */
    public function searchAction(): ResponseInterface
    {
        $sword = '';
        $results = [];
        if ($this->request->hasArgument('sword')) {
            $sword = $this->request->getArgument('sword');

            if (strlen($sword) > 2) {
                $results = $this->organisationRepository->findByZipOrCity($sword);
            }
        }

        // if more than 1 element show list view else goto detail
        if (count($results) === 1 && $this->settings['open_result'] == 1) {
            $result = current($results);

            if (isset($result['orgType']) && in_array($result['orgType'], $this->orgPidKeys)) {
                $pid = $this->settings[$this->orgPidKeys[$result['orgType']]];
            } else {
                // fallback if no orgTyp is given
                $pid = $this->settings['pIdKv'];
            }

            /** generate redirect uri */
            $this->uriBuilder->reset()->setCreateAbsoluteUri(true);
            $this->uriBuilder->setTargetPageUid((int)$pid);

            $uri = $this->uriBuilder->uriFor('detail', ['organisation' => current($results)['orgID']], 'OrganisationList');

            return $this->responseFactory->createResponse(307)
                ->withHeader('Location', $uri);

        } else {

            /**
             * Set pagination
             */
            $itemsPerPage = 10;
            $maximumLinks = 15;

            $currentPage = $this->request->hasArgument('currentPage') ? (int)$this->request->getArgument('currentPage') : 1;
            $paginator = new ArrayPaginator($results, $currentPage, $itemsPerPage);
            $pagination = new NumberedPagination($paginator, $maximumLinks);
            $this->view->assign('pagination', [
                'paginator' => $paginator,
                'pagination' => $pagination,
            ]);

            $this->view->assignMultiple([
                'data' => $this->configurationManager->getContentObject()->data,
                'sword' => trim($sword),
                'results' => $results,
                'error' => !empty($sword) && strlen($sword) < 3,
            ]);
        }
        return $this->htmlResponse();
    }
}
