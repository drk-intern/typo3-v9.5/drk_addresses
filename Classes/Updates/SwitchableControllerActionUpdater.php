<?php
namespace DrkService\DrkAddresses\Updates;

use DRK\DrkGeneral\Updates\AbstractSwitchableControllerActionsPluginUpdater;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drk_adresses-SwitchableControlleractionUpdater')]
class SwitchableControllerActionUpdater extends AbstractSwitchableControllerActionsPluginUpdater
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'drkaddresses_organisationplugin',
            'switchableControllerActions' => 'Organisation->list;Organisation->detail',
            'targetListType' => '',
            'targetCtype' => 'drkaddresses_organisationlist'
        ],
        [
            'sourceListType' => 'drkaddresses_organisationplugin',
            'switchableControllerActions' => 'Organisation->overview;Organisation->detail',
            'targetListType' => '',
            'targetCtype' => 'drkaddresses_organisationoverview'
        ],
        [
            'sourceListType' => 'drkaddresses_organisationplugin',
            'switchableControllerActions' => 'Organisation->search;Organisation->detail',
            'targetListType' => '',
            'targetCtype' => 'drkaddresses_organisationsearch'
        ],
        [
            'sourceListType' => 'drkaddresses_organisationplugin',
            'switchableControllerActions' => 'Organisation->sister;Organisation->detail',
            'targetListType' => '',
            'targetCtype' => 'drkaddresses_organisationsister'
        ],
        [
            'sourceListType' => 'drkaddresses_organisationplugin',
            'switchableControllerActions' => 'Organisation->company;Organisation->detail',
            'targetListType' => '',
            'targetCtype' => 'drkaddresses_organisationcompany'
        ],
    ];
}
