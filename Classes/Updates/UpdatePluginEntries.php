<?php
declare(strict_types=1);

namespace DrkService\DrkAddresses\Updates;

/**
 * This file is part of the "news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Doctrine\DBAL\FetchMode;
use InvalidArgumentException;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

/**
 * Fills sys_category.slug with a proper value
 */

#[UpgradeWizard('drkaddressesupdatePluginEntries')]
class UpdatePluginEntries extends AbstractRecordUpdater implements UpgradeWizardInterface
{
    protected $table = 'tt_content';

    /**
     * @return string Title of this updater
     */
    public function getTitle(): string
    {
        return 'Adjust tt_content records for new plugin name (drkaddresses)';
    }

    /**
     * @return string Longer description of this updater
     */
    public function getDescription(): string
    {
        return 'Adjust tt_content records for new plugin name';
    }

    /**
     * Performs the accordant updates.
     *
     * @return bool Whether everything went smoothly or not
     */
    public function executeUpdate(): bool
    {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($this->table);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->like('list_type', '"%drkadressen_organisationplugin%"')
            )
            ->execute()
            ->fetchAll(FetchMode::ASSOCIATIVE);

        foreach ($results as $result) {
            $result['list_type'] = str_replace(
                'drkadressen_organisationplugin',
                'drkaddresses_organisationplugin',
                $result['list_type']
            );
            $result['pi_flexform'] = str_replace(';Organisation-&gt;proposal', '', $result['pi_flexform']);
            $result['pi_flexform'] = str_replace(';Organisation-&gt;sendProposal', '', $result['pi_flexform']);
            $connection->update(
                $this->table,
                [
                    'list_type' => $result['list_type'],
                    'pi_flexform' => $result['pi_flexform']
                ],
                [
                    'uid' => $result['uid']
                ]
            );
        }
        return true;
    }

    /**
     * Check if there are record within database table with an empty "slug" field.
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    protected function checkIfWizardIsRequired(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable($this->table);
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->like('list_type', '"%drkadressen_organisationplugin%"')
            )
            ->executeQuery()
            ->fetchOne();
        return $numberOfEntries > 0;
    }
}
