<?php

namespace DrkService\DrkAddresses\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Nicole Cordes <cordes@cps-it.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Closure;
use InvalidArgumentException;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class BlockViewHelper extends AbstractViewHelper
{
    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * initializeArguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('subject', 'array', 'Subject');
        $this->registerArgument('as', 'string', 'As');
        $this->registerArgument('chunks', 'integer', 'Number of chunks');
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function render()
    {
        return static::renderStatic(
            $this->arguments,
            $this->buildRenderChildrenClosure(),
            $this->renderingContext
        );
    }

    /**
     * @param array $arguments
     * @param Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $chunks = $arguments['chunks'];
        if ($chunks < 1) {
            throw new InvalidArgumentException('Chunks need to be a valid positive integer', 1452633481285);
        }

        $subject = $arguments['subject'];
        $subjectCount = count($subject);
        if (empty($subjectCount)) {
            return '';
        }

        $offset = 0;
        $output = '';
        $templateVariableContainer = $renderingContext->getVariableProvider();
        do {
            $chunkCount = ceil($subjectCount / $chunks);
            $chunk = array_slice($subject, $offset, $chunkCount);

            $templateVariableContainer->add($arguments['as'], $chunk);
            $output .= $renderChildrenClosure();
            $templateVariableContainer->remove($arguments['as']);

            $offset += $chunkCount;
            $subjectCount -= $chunkCount;
            $chunks--;
        } while ($chunks);

        return $output;
    }
}
