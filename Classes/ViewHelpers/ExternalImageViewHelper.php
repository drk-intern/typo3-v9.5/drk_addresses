<?php

namespace DrkService\DrkAddresses\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Nicole Cordes <cordes@cps-it.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Core\Page\AssetCollector;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\Exception;
use TYPO3\CMS\Frontend\Imaging\GifBuilder;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class ExternalImageViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * @var array
     */
    protected $settings;

    /**
     * @var string
     */
    protected $tagName = 'img';

    /**
     * Initialize arguments.
     *
     * @return void
     * @throws Exception
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerTagAttribute('alt', 'string', 'Specifies an alternate text for an image', false);
        $this->registerTagAttribute(
            'ismap',
            'string',
            'Specifies an image as a server-side image-map. Rarely used. Look at usemap instead',
            false
        );
        $this->registerTagAttribute(
            'longdesc',
            'string',
            'Specifies the URL to a document that contains a long description of an image',
            false
        );
        $this->registerTagAttribute('usemap', 'string', 'Specifies an image as a client-side image-map', false);
        $this->registerArgument('interchangeSettings', '\\Array', 'and change settings', false, []);
        $this->registerArgument('falImage', FileReference::class, 'Image Resource', false, null);
        $this->registerArgument(
            'src',
            'string',
            'a path to a file, a combined FAL identifier or an uid (int). If $treatIdAsReference is set, the integer is considered the uid of the sys_file_reference record. If you already got a FAL object, consider using the $image parameter instead',
            false,
            null
        );
        $this->registerArgument(
            'width',
            'string',
            'width of the image. This can be a numeric value representing the fixed width of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.',
            false,
            null
        );
        $this->registerArgument(
            'height',
            'string',
            'height of the image. This can be a numeric value representing the fixed height of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.',
            false,
            null
        );
        $this->registerArgument('minWidth', 'int', 'minimum width of the image', false, null);
        $this->registerArgument('maxWidth', 'int', 'maximum width of the image', false, null);
        $this->registerArgument('minHeight', 'int', 'minimum height of the image', false, null);
        $this->registerArgument('maxHeight', 'int', ' maximum height of the image', false, null);
        $this->registerArgument(
            'treatIdAsReferenc',
            'bool',
            'given src argument is a sys_file_reference record',
            false,
            null
        );
        $this->registerArgument(
            'crop',
            'bool|string',
            'overrule cropping of image (setting to FALSE disables the cropping set in FileReference)',
            false,
            null
        );
        $this->registerArgument('absolute', 'bool', 'Force absolute URL', false, null);
        $this->registerArgument('fallback', 'string', 'Fallback Image', false);
    }

    /**
     * @return array
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }

    /**
     * Resizes a given external image (if required) and renders the respective img tag
     *
     * @return string
     */
    public function render(): string
    {
        /** @var GifBuilder $gifBuilder */
        $gifBuilder = GeneralUtility::makeInstance(GifBuilder::class);

        $pathSite = Environment::getPublicPath();


        $src = $this->arguments['src'];
        $width = $this->arguments['width'];
        $height = $this->arguments['height'];
        $minWidth = $this->arguments['minWidth'];
        $minHeight = $this->arguments['minHeight'];
        $maxWidth = $this->arguments['maxWidth'];
        $maxHeight = $this->arguments['maxHeight'];
        $absolute = $this->arguments['absolute'];

        // choose fallback image
        if (empty($this->arguments['fallback'])) {
            $fallback = PathUtility::getPublicResourceWebPath (
                'EXT:drk_addresses/Resources/Public/Images/default_map.jpg');
        }
        else {
            if ( str_starts_with($this->arguments['fallback'], 'EXT:')) {
                $fallback = PathUtility::getPublicResourceWebPath(
                    $this->arguments['fallback']);
            }
            else {
                $fallback = $this->arguments['fallback'];
            }
        }

        $configuration = [
            'width' => $width,
            'height' => $height,
            'minWidth' => $minWidth,
            'minHeight' => $minHeight,
            'maxWidth' => $maxWidth,
            'maxHeight' => $maxHeight,
        ];

        $typo3tempDirectory = 'pics/drk_addresses/' . $this->renderingContext->getControllerName() . '/';
        GeneralUtility::mkdir_deep($pathSite . '/typo3temp/' . $typo3tempDirectory);
        $processedImageExtension = $this->getTargetImageExtension($src);
        $processedImageBaseName = substr(md5($src . serialize($configuration)), 0, 10);
        $processedImageName = $pathSite . '/typo3temp/' . $typo3tempDirectory . $processedImageBaseName . '.' . $processedImageExtension;
        $processedImageWidth = $width;
        $processedImageHeight = $height;
        if (file_exists($processedImageName)) {
            $imageInformation = $gifBuilder->getImageDimensions($processedImageName);
            $processedImageWidth = $imageInformation[0];
            $processedImageHeight = $imageInformation[1];
        } else {

            // get image from external src
            if ($image = GeneralUtility::getUrl($src)) {
                $temporaryImageExtension = strtolower(PathUtility::pathinfo($src, PATHINFO_EXTENSION));
                $temporaryImageName = GeneralUtility::tempnam(
                    $this->renderingContext->getControllerName(),
                    '.' . $temporaryImageExtension
                );

                GeneralUtility::writeFileToTypo3tempDir($temporaryImageName, $image);

                $imageInformation = $gifBuilder->imageMagickConvert(
                    $temporaryImageName,
                    $processedImageExtension,
                    $configuration['width'],
                    $configuration['height'],
                    '',
                    '',
                    [
                        'minW' => $minWidth,
                        'minH' => $minHeight,
                        'maxW' => $maxWidth,
                        'maxH' => $maxHeight,
                    ]
                );

                if (is_array($imageInformation)) {
                    rename($imageInformation[3], $processedImageName);
                    $processedImageWidth = $imageInformation[0];
                    $processedImageHeight = $imageInformation[1];
                }
                GeneralUtility::unlink_tempfile($temporaryImageName);
            } else { // if no image available take a fallback picture
                $processedImageName = ($pathSite . $fallback);
            }
        }

        $this->tag->addAttribute('src', $this->getImageUri($processedImageName, $absolute));
        $this->tag->addAttribute('width', $processedImageWidth);
        $this->tag->addAttribute('height', $processedImageHeight);

        // The alt-attribute is mandatory to have valid html-code, therefore add it even if it is empty
        if (empty($this->arguments['alt'])) {
            $this->tag->addAttribute('alt', '');
        }

        return $this->tag->render();
    }

    /**
     * Returns the file extension type for processed images according to configuration
     *
     * @param string $image
     * @return string
     */
    protected function getTargetImageExtension($image)
    {
        $targetImageExtensionConfiguration = $GLOBALS['TYPO3_CONF_VARS']['GFX']['thumbnails_png'];
        $currentImageExtension = strtolower(PathUtility::pathinfo($image, PATHINFO_EXTENSION));
        if ($currentImageExtension === 'jpg' || $currentImageExtension === 'jpeg') {
            if ($targetImageExtensionConfiguration == 2) {
                $targetImageExtension = 'gif';
            } elseif ($targetImageExtensionConfiguration == 3) {
                $targetImageExtension = 'png';
            } else {
                $targetImageExtension = 'jpg';
            }
        } else {
            // check if a png or a gif should be created
            if ($targetImageExtensionConfiguration == 1 || $currentImageExtension === 'png') {
                $targetImageExtension = 'png';
            } else {
                // thumbnails_png is "0"
                $targetImageExtension = 'gif';
            }
        }

        return $targetImageExtension;
    }

    /**
     * Get public url of image depending on the environment
     *
     * @param string $image
     * @param bool $absolute
     * @return string
     */
    public function getImageUri($image, $absolute)
    {
        if (PathUtility::isAbsolutePath($image)) {
            $image = PathUtility::stripPathSitePrefix($image);
        }
        $parsedUrl = parse_url($image);

        // No prefix in case of an already fully qualified URL
        if (isset($parsedUrl['host'])) {
            $uriPrefix = '';
        } elseif (ApplicationType::fromRequest($GLOBALS['TYPO3_REQUEST'])->isFrontend()) {
            $uriPrefix = $GLOBALS['TSFE']->absRefPrefix;
        } else {
            $uriPrefix = GeneralUtility::getIndpEnv('TYPO3_SITE_PATH');
        }

        if ($absolute) {
            if (isset($parsedUrl['host']) && !isset($parsedUrl['scheme'])) {
                $uriPrefix = (GeneralUtility::getIndpEnv('TYPO3_SSL') ? 'https:' : 'http:') . $uriPrefix;
            }

            return GeneralUtility::locationHeaderUrl($uriPrefix . $image);
        }

        return $uriPrefix . $image;
    }
}
