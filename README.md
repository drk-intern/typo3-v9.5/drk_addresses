# DRK Adressen
## Worum geht es?

Diese Erweiterung bietet Ihnen die Möglichkeit, Adressen und Angebotsseiten aller Gliederungen des Deutschen Roten Kreuzes in verschiedenen Formen darzustellen.
Gliederungen können nach PLZ, Ort und Verbandsname gesucht werden.

Folgende Gliederung nach Adressen und Angeboten sind möglich:

* nach Landesverbänden
* nach Kreisverbänden
* nach Schwesternschaften
* nach (g)GmbHs

##Screenshots
Übersicht Landesverbände (Abbildung 1):

![EM](Documentation/Images/Landesverbaende.png)

Alphabetische Liste der Gliederungen (Abbildung 2):

![EM](Documentation/Images/Gliederungsliste.png)

Details einer Gliederung (Abbildung 3):

![EM](Documentation/Images/Gliederungsdetail.png)

Adressfinder (Abbildung 4):

![EM](Documentation/Images/Adressfinder.png)

##Benutzerhandbuch

Die Zielgruppe der Benutzer sind Administratoren und/oder Chefredakteure. Ziel ist es ein zentrales Adressverzeichnis über alle Landesverbände, Kreisverbände, Schwesternschaften und (g)GmbHs des Deutschen Roten Kreuzes zur auf der Webseite Verfügung zu stellen.
Hierzu wird ein neues Inhaltselement vom Typ Plug-Ins → „DRK Adressen“ angelegt.

![EM](Documentation/Images/Plugin.png)

Die Eingangsanzeige kann jeweils vom Typ „Liste Kreisverbände“, „Übersicht Landesverbände“, „Suche“, „Übersicht Schwesternschaften“ und „Liste GmbHs“ definiert werden.

###Typ „Liste Kreisverbände“

Wie in der Abbildung 2, werden alle Kreisverbände alphabetische selektiert in einer Liste dargestellt. Über den jeweiligen Link erreicht man die Detaildarstellung (Abbildung 3), welche alle Informationen der Gliederung wie Stammdaten, Gebietskarte,  Webseiten der Dienstleistungen und Einrichtungen im Überblick, sowie Homepages der Einrichtung darstellt.

###Typ „Übersicht Landesverbände“

Alle Landesverbände werden neben einer Gebietskarte wie in Abbildung 1 aufgelistet. Über den Link der Gliederung erreicht man die Details. Die Gebietskarte enthält ein Imagemap, welches über das Templates bei Bedarf gepflegt werden kann.

###Typ „Übersicht Schwesternschaften“
Die Detaildarstellung des „Verbandes der Schwesternschaften“ wird dargestellt.

###Typ „Liste GmbHs“

Äquivalent zur „Liste Kreisverbände“ werden hier alle (g)GmbHs in einer alphabetisch selektierten Liste dargestellt.

###Typ „Suche“

„Suche“ stellt eine Suchmaske (Abbildung 4) zur Verfügung. Über diese Suchmaske können per PLZ, Orte und Gliederungsnamen Gliederungen gefunden werden.


##Konfiguration

| Property| Data type | Description | Default |
| :------------- |:-------------| :---------------| :--------------------------------------------------- |
| pIdLv | integer | PageID der Seite der Landesverbände | 413 |
| pIdKv | integer | PageID der Seite der Kreisverbände | 414 |
| pIdSister | integer | PageID der Seite der Schwesternschaften | 415 |
| pIdCompany | integer | PageID der Seite der gGmbH | 414 |
| ws_dldb_wsdl | string | Ort der WSDL-Datei | https://webservice.drk-db.de/soap/service/wsdl/organisation |
| default_map | string | Pfad zu alternativer Karte, falls keine Karte vorhanden ist. | typo3conf/ext/drk_addresses/Resources/Public/Images/default_map.jpg |
| default_sister_map | string | Pfad zu alternativer Karte, falls keine Karte vorhanden ist. | typo3conf/ext/drk_addresses/Resources/Public/Images/default_sister_map.jpg |
| open_result | boolean | Bei einem Ergebnis, wird es sofort geöffnet. | TRUE |
| pIdSearchResult | integer | PageID des Suchergebnisses - wird benötigt, wenn sie die Suchmaske und Anzeige der Suchergebnisse trennen möchten. | 0 |
| mapType | options | Kartenanbieter, OpenStreetMap oder GoogleMaps | osm |
| google_api_key | string | Google API Schlüssel |  |
| google_maps_api_url | string | URL zur Google-API | https://maps.googleapis.com/maps/api/js |


###RealURL Setup

```
'postVarSets' => array(
		'_DEFAULT' => array(
			'liste' => array(
            // EXT:drk_adressen start
                'liste' => array(
                    array(
                        'GETvar' => 'tx_drkadressen_organisationplugin[section]',
                    ),
                ),
                'ueberblick' => array(
                    array(
                        'GETvar' => 'tx_drkadressen_organisationplugin[overview]',
                        'valueDefault' => '1',
                    ),
                ),
                'detail' => array(
                    array(
                        'GETvar' => 'tx_drkadressen_organisationplugin[organisation]',
                    ),
                ),
                'suche' => array(
                    array(
                        'GETvar' => 'tx_drkadressen_organisationplugin[search]',
                        'valueDefault' => 'seite',
                    ),
                    array(
                        'GETvar' => 'tx_drkadressen_organisationplugin[page]',
                    ),
                ),
                'seite' => array(
                    array(
                        'GETvar' => 'tx_drkadressen_organisationplugin[page]',
                    ),
                ),
            // EXT:drk_adressen end
            ),
        )
```
